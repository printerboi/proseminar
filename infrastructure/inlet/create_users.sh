#!/bin/bash


if [ ! -d "proseminar" ]; then
	#If the repo directory does not exist...
	git clone https://gitlab.com/printerboi/proseminar.git
fi

#Simple function to add a user, set his password and copy the provided repo to their home directory
function createUser() {
	#Test if the home directory already exsits.
	if [ ! -d "/home/$1/" ]
	then
		useradd $1 -m -s /bin/bash
	else
		useradd $1 -s /bin/bash
	fi
	echo "$1:$USERPASSWD" | chpasswd
	cp -R ./proseminar/main_presentation/ "/home/$1/"
	chown -R "$1:$1" "/home/$1/main_presentation"
}

#Get the provided flags
while getopts u: flag
do
    case "${flag}" in
        u) username=${OPTARG};;
    esac
done

#Tes if a username was provided
if test -z $username
then
	#if not just use the userlist.txt
	for user in `more userlist.txt`
	do
		echo $user

		#Test if the user already exists
		if id "$user" &>/dev/null; then
			echo "	-->Skipping $user, the user already exists..."
		else
			createUser $user
		fi
	done
else
	#if so just add the given username
	if id "$user" &>/dev/null; then
		echo "User $user already exists. Doing nothing..."
	else
		createUser $username
	fi
fi


