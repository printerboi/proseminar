#!/bin/bash

echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

apt-get update
apt-get upgrade -y
apt-get install -y git
apt-get install -y curl
apt-get install -y cargo

