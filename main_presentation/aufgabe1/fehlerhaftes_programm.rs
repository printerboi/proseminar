use std::io;

fn main(){
    let pi: f32 = 3.14;
    let engineer_pi: i8 = 3.0;
   
    println("Willkommen zum Kreisflächenrechner 3000.\n\nBitte geben Sie einen Radius ein: ");
    let radius = String::new();
    let mut engineer = String::new();
    io::stdin()
        .read_line(&mut radius)
        .expect("Fehler beim einlesen")
    
    println!("Vielen Dank. Eine Frage noch: Sind Sie Maschinenbauer?(ja/nein)");
    io::stdin()
        .read_line(&mut engineer)
        .expect("Fehler beim einlesen");

    let mut result: f32 = '';
    let parsed_radius: f32 = radius.trim().parse::<f32>().unwrap();

    if (engineer.trim() == "ja") {
        result = parsed_radius / engineer_pi;
        println!("{parsed_radius}");
    }else if engineer.trim() == "nein" {
        result = parsed_radius * pi;        
    }

    println!("Ihr Kreis hat eine Fläche von {result} Flächeneinheiten")
}
