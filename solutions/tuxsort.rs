
fn main(){
    let mut arr: [i32; 10] = [69, 85, 22, 20, 32, 11, 14, 50, 76, 55];
    print_array(arr);
    arr = tuxsort(arr);
    print_array(arr);
}

fn tuxsort(mut arr: [i32; 10]) -> [i32; 10]{
    let mut index: usize = 0;

    while index < arr.len() {
        if index == 0 {
            index += 1;
        }

        if arr[index] >= arr[index-1] {
            index += 1;
        }else{
            let c = arr[index-1];
            arr[index-1] = arr[index];
            arr[index] = c;
            index -= 1;
        }
    }

    return arr;
}

fn print_array(arr: [i32; 10]){
    print!("[");

    let mut i: usize = 0;
    for n in arr {
        print!("{n}");
        
        if i != arr.len()-1 {
            print!(", ");
        }
        i += 1;
    }
    print!("]\n");
}
